namespace itertools {

	template<typename T> 
	struct range {
		T from, to;
		range(T from, T to): from(from), to(to) {}
		struct iterator {
			T current;
			bool forward;
			T operator*() { 
				return current; 
			}
			iterator& operator++() { 
				if (forward) ++current; 
				else --current;
				return *this; 
			}
			bool operator==(const iterator& other) { return current==other.current; }
			bool operator!=(const iterator& other) { return current!=other.current; }
		};
		iterator begin() const { return from<to? iterator{from,true}: iterator{from,false}; }
		iterator end()   const { return from<to? iterator{to,true}: iterator{to,false}; }
	};
}
