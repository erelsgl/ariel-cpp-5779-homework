namespace itertools {
	template<typename T1, typename T2>
	struct chain {
		const T1& first;
		const T2& second;
		chain(const T1 f, const T2 s): 
			first(f), second(s) {}
		struct iterator {
			decltype(first.begin()) firstIterator, firstEnd;
			decltype(second.begin()) secondIterator, secondEnd;
			bool areWeInFirst=true;
			decltype(*first.begin()) operator*() {
				if (areWeInFirst) {
					return *firstIterator;
				} else {
					return *secondIterator;
				}
			}

			iterator& operator++() { 
				if (areWeInFirst) {
					++firstIterator;
					if (firstIterator==firstEnd) {
						areWeInFirst=false;
					}
				} else {
					++secondIterator;
				}
				return *this; 
			}
			iterator operator++(int) {
				iterator tmp=*this;
				operator++();
				return tmp;
			}

			bool operator==(const iterator& other) { 
				if (areWeInFirst) {
					return firstIterator==other.firstIterator;
				} else {
					return secondIterator==other.secondIterator;
				}
			}
			bool operator!=(const iterator& other) { 
				return !(operator==(other));
			}
		};

		iterator begin() const { 
			return iterator{first.begin(), first.end(), second.begin(), second.end(), true}; 
		}
		iterator end()  const { 
			return iterator{first.end(), first.end(), second.end(), second.end(), false}; 
		}
	};
}
