#include <utility>
#include <iostream>
#include "output.hpp"

namespace itertools {
	template<typename T1, typename T2>
	struct zip {
		const T1 first;
		const T2 second;
		zip(const T1& first, const T2& second): first(first), second(second) {}
		typedef std::pair<decltype(*first.begin()), decltype(*second.begin())> PAIR;
		struct iterator {
			decltype(first.begin()) firstIterator, firstEnd;
			decltype(second.begin()) secondIterator, secondEnd;
			PAIR operator*() {
				return PAIR(*firstIterator, *secondIterator);
			}
			iterator& operator++() { 
				++firstIterator;
				++secondIterator;
				return *this; 
			}
			iterator operator++(int) {
				iterator tmp=*this;
				operator++();
				return tmp;
			}

			bool operator==(const iterator& other) { 
				return firstIterator==other.firstIterator && secondIterator==other.secondIterator;
			}
			bool operator!=(const iterator& other) { 
				return !(operator==(other));
			}
		};

		iterator begin() const { 
			return iterator{first.begin(), first.end(), second.begin(), second.end()}; 
		}
		iterator end()  const { 
			return iterator{first.end(), first.end(), second.end(), second.end()}; 
		}
	};

}
