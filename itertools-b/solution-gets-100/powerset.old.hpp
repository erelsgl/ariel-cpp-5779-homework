#include <set>
#include <iostream>
#include <type_traits>
#include "output.hpp"

namespace itertools {

	template<typename T> 
	struct powerset {
		const T world;
		typedef decltype(world.begin()) IT;
		typedef typename std::remove_const<typename std::remove_reference<decltype(*world.begin())>::type>::type ELEM;
		IT worldbegin, worldend;
		uint worldsize;

		powerset(const T& world): world(world) {
			worldbegin = world.begin();
			worldend = world.end();
			worldsize=0;
			for (IT i=worldbegin; i!=worldend; ++i)
				++worldsize;
		}

		struct iterator {
			IT worldbegin, worldend;
			int subset_index;
			std::set<ELEM> operator*() { 
				std::set<ELEM> result;
				uint i=0;
				for (IT it=worldbegin; it!=worldend; ++it) {
					uint bit = (1 << i);
					// cout << "bit=" << bit<<" subset_index=" << subset_index << endl;
					if (subset_index & bit) {
						result.insert(*it);
					}
					++i;
				}
				return result;
			}
			iterator& operator++() { 
				++subset_index; 
				return *this; 
			}
			bool operator==(const iterator& other) { 
				return subset_index==other.subset_index; 
			}
			bool operator!=(const iterator& other) { 
				return subset_index!=other.subset_index; 
			}
		};
		iterator begin() const { 
			return iterator{worldbegin, worldend, 0}; 
		}
		iterator end()   const { 
			return iterator{worldbegin, worldend, 1<<worldsize}; 
		}
	};
}
