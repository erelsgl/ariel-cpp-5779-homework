namespace itertools {
	template<typename T1, typename T2>
	struct chain {
		chain(const T1& first, const T2& second): first(first), second(second) {
			using V1 = const decltype(*first.begin())&;
			using V2 = const decltype(*second.begin())&;
			static_assert(
				is_same<V1,V2>::value,
				"The two arguments of 'chain' must have the same value type!");
		}
		const T1 first;
		const T2 second;
		struct iterator {
			decltype(first.begin()) firstIterator, firstEnd;
			decltype(second.begin()) secondIterator, secondEnd;
			bool areWeInFirst=true;
			decltype(*first.begin()) operator*() {
				if (areWeInFirst) {
					return *firstIterator;
				} else {
					return *secondIterator;
				}
			}

			iterator& operator++() { 
				if (areWeInFirst) {
					++firstIterator;
					if (firstIterator==firstEnd) {
						areWeInFirst=false;
					}
				} else {
					++secondIterator;
				}
				return *this; 
			}
			iterator operator++(int) {
				iterator tmp=*this;
				operator++();
				return tmp;
			}

			bool operator==(const iterator& other) { 
				if (areWeInFirst) {
					return firstIterator==other.firstIterator;
				} else {
					return secondIterator==other.secondIterator;
				}
			}
			bool operator!=(const iterator& other) { 
				return !(operator==(other));
			}
		};

		iterator begin() const { 
			return iterator{first.begin(), first.end(), second.begin(), second.end(), true}; 
		}
		iterator end()  const { 
			return iterator{first.end(), first.end(), second.end(), second.end(), false}; 
		}
	};
}
