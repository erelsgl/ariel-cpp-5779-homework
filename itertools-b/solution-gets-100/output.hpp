#pragma once
#include <utility>
#include <set>
#include <iostream>

template<typename T1, typename T2>
std::ostream& operator<<(std::ostream& out, const std::pair<T1,T2>& pair) {
	out << pair.first << "," << pair.second;
	return out;
}


template<class T>
std::ostream& operator<<(std::ostream& out, const std::set<T>& subset) {
	out << "{";
	bool first=true;
	for (T val: subset) {
		if (!first) out << ",";
		first=false;
		out << val;
	}
	out << "}";
	return out;
}
