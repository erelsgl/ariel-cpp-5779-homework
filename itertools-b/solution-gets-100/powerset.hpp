#include <set>
#include <iostream>
#include <type_traits>
#include "output.hpp"

namespace itertools {

	// A struct that represents a subset, but uses only O(1) memory
	template<typename IT, typename ELEM>
	struct subset {
		IT worldbegin, worldend;
		int subset_index;

		// iterator over elements in a subset
		struct subset_iterator {
			IT worldbegin, worldend;
			int subset_index;
			IT element_iterator;
			uint bit;
			void go_to_next_included_element() {
				while (!(subset_index & bit) && element_iterator!=worldend) {
					++element_iterator;
					bit<<=1;
				}
			}
			ELEM operator*() { 
				return *element_iterator;
			}
			subset_iterator& operator++() {
				++element_iterator;
				bit<<=1;
				go_to_next_included_element();
				return *this; 
			}
			bool operator==(const subset_iterator& other) { 
				return element_iterator==other.element_iterator; 
			}
			bool operator!=(const subset_iterator& other) { 
				return element_iterator!=other.element_iterator; 
			}
		};   // end struct subset_iterator
		subset_iterator begin() const { 
			subset_iterator b {worldbegin, worldend, subset_index, worldbegin, 1};
			b.go_to_next_included_element();
			return b;
		}
		subset_iterator end()   const { 
			return subset_iterator{worldbegin, worldend, subset_index, worldend, 0};
		}
	};  // end struct subset


	template<typename IT, typename ELEM>
	void print(std::ostream& out, const subset<IT, ELEM>& subset) {
		out << "{";
		bool first=true;
		for (ELEM val: subset) {
			if (!first) out << ",";
			first=false;
			out << val;
		}
		out << "}";
	}

	template<typename IT, typename ELEM>
	std::ostream& operator<<(std::ostream& out, const subset<IT, ELEM>& subset) {
		print(out, subset);
		return out;
	}


	template<typename T> 
	struct powerset {
		const T world;
		typedef decltype(world.begin()) IT;
		typedef typename std::remove_const<typename std::remove_reference<decltype(*world.begin())>::type>::type ELEM;
		IT worldbegin, worldend;
		// equivalent to:
		//decltype(world.begin()) worldbegin, worldend;
		
		uint worldsize;

		powerset(const T& world): world(world) {
			worldbegin = world.begin();
			worldend = world.end();
			worldsize=0;
			for (IT i=worldbegin; i!=worldend; ++i)
				++worldsize;
		}

		struct iterator {
			IT worldbegin, worldend;
			int subset_index;
			auto operator*() { 
				return subset<IT,ELEM>{worldbegin, worldend, subset_index};
			}
			iterator& operator++() { 
				++subset_index; 
				return *this; 
			}
			bool operator==(const iterator& other) { 
				return subset_index==other.subset_index; 
			}
			bool operator!=(const iterator& other) { 
				return subset_index!=other.subset_index; 
			}
		};  // end struct iterator

		iterator begin() const { 
			return iterator{worldbegin, worldend, 0}; 
		}
		iterator end()   const { 
			return iterator{worldbegin, worldend, 1<<worldsize}; 
		}
	}; // end struct powerset


}  // end namespace itertools



