#include <utility>
#include <iostream>
#include "output.hpp"

namespace itertools {
	template<typename T1, typename T2>
	struct product {
		const T1 first;
		const T2 second;
		product(const T1& first, const T2& second): first(first), second(second) {}
		typedef std::pair<decltype(*first.begin()), decltype(*second.begin())> PAIR;
		struct iterator {
			decltype(first.begin()) firstBegin, firstIterator, firstEnd;
			decltype(second.begin()) secondBegin, secondIterator, secondEnd;
			PAIR operator*() {
				return PAIR(*firstIterator, *secondIterator);
			}
			iterator& operator++() {
				++secondIterator;
				if (secondIterator==secondEnd) {
					if (firstIterator!=firstEnd) {
						++firstIterator;
						secondIterator=secondBegin;
					}
				}
				return *this; 
			}
			iterator operator++(int) {
				iterator tmp=*this;
				operator++();
				return tmp;
			}

			bool operator==(const iterator& other) { 
				return firstIterator==other.firstIterator && secondIterator==other.secondIterator;
			}
			bool operator!=(const iterator& other) { 
				return !(operator==(other));
			}
		};

		iterator begin() const { 
			if (first.begin()==first.end() || second.begin()==second.end())
				return end(); // special case - empty product
			else
				return iterator{first.begin(), first.begin(), first.end(), second.begin(), second.begin(), second.end()}; 
		}
		iterator end()  const { 
			    return iterator{first.begin(), first.end(),   first.end(), second.begin(), second.begin(), second.end()};
		}
	};

}
