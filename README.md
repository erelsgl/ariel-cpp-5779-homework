# ariel-cpp-5779-homework

Automatic grading files for homework in C++ course, year 5779.

Each subfolder represents a single homework assignment.
The subfolder contains an executable script named "grade", which is responsible for calculating the student's grade.

Our automatic system (called "badkan") uses the following algorithm to calculate the grade:

1. Clone the student's solution from github;

2. Copy into the the student's folder, all the files in the assignment folder;

3. Run the script "grade" in the student's folder.

Here is the algorithm for creating a new assignment:

1. Create a new subfolder;

2. Create all example files that you need in order to automatically test the student's solution;

3. Create an executable script named "grade" that calculates the student's grade.

4. To test your automatic grading files, you can create a partial solution in a subfolder of your assignment subfolder. Then, go into this subfolder and run the scirpt ../../test-solution.

For simple examples, see the assignments "multiply" and "reverse".
