#include <iostream>
using std::ostream;
using std::istream;

class CircularInt {
	int _min, _max;   // smallest and largest possible values
	int _count;       // how many different values are there (auxiliary field)
	int _now;         // current value;

	int normalized(int num) const;

	// inits _min to min, _max to max, and _now to now.
	// Does NOT normalize now (this is why it is private).
	CircularInt(int min, int max, int now);

public: 
	int now() const { 
		return _now; 
	}

	// inits _min to min, _max to max, and _now to _min.
	CircularInt(int min, int max): 
		CircularInt(min, max, min) { }

	const CircularInt modified(int newNow) const {
		return CircularInt(_min, _max, normalized(newNow));
	}

	CircularInt& modified(int newNow) {
		_now = normalized(newNow);
		return *this;
	}

	CircularInt& operator= (int i) {
		return modified(i);
	}

	const CircularInt operator--(int) {
		int _previous = _now;
		_now = normalized(_now - 1);
		return CircularInt(_min,_max,_previous);
	}
    CircularInt& operator--() {
		return modified(_now - 1); 
	}

	const CircularInt operator++(int) {
		int _previous = _now;
		_now = normalized(_now + 1);
		return CircularInt(_min,_max,_previous);
	}
    CircularInt& operator++() {
		return modified(_now + 1); 
	}

	CircularInt operator-() const {
		return CircularInt{_min, _max, normalized(-_now)};
	}

	bool operator==(const CircularInt& other) {
		return _min==other._min && _max==other._max && _now==other. _now; 
	}

	operator int() const { return _now; }

	friend const CircularInt operator/(const CircularInt& c, int n);
};

inline ostream& operator<<(ostream& out, const CircularInt& c) {
	return (out << c.now()); }

inline istream& operator>>(istream& in, CircularInt& c) {
	int newNow; 
	in >> newNow;
	c.modified(newNow);
	return in;
}


inline const CircularInt operator+(int n, const CircularInt& c) {
	return c.modified(n + c.now()); }
inline const CircularInt operator+(const CircularInt& c, int n) {
	return c.modified(n + c.now()); }
inline const CircularInt operator+(const CircularInt& c, CircularInt& d) {
	return c.modified(d.now() + c.now()); }
inline CircularInt& operator+=(CircularInt& c, int n) {
	return c.modified(c.now() + n); }

inline const CircularInt operator-(int n, const CircularInt& c) {
	return c.modified(n - c.now()); }
inline const CircularInt operator-(const CircularInt& c, int n) {
	return c.modified(c.now() - n); }
inline const CircularInt operator-(const CircularInt& c, CircularInt& d) {
	return c.modified(d.now() - c.now()); }
// inline CircularInt& operator-=(CircularInt& c, int n) {
// 	return c.modified(c.now() - n); }

inline const CircularInt operator*(int n, const CircularInt& c) {
	return c.modified(n * c.now()); }
inline const CircularInt operator*(const CircularInt& c, int n) {
	return c.modified(c.now() * n); }
inline const CircularInt operator*(const CircularInt& c, CircularInt& d) {
	return c.modified(d.now() * c.now()); }
inline CircularInt& operator*=(CircularInt& c, int n) {
	return c.modified(c.now() * n); }
	
inline const CircularInt operator/(int n, const CircularInt& c) {
	return c.modified(n) / c.now(); }
inline CircularInt& operator/=(CircularInt& c, int n) {
	return c.modified((c/n).now()); }


inline bool operator==(int n, const CircularInt& c) {
	return c.now()==n; }

inline bool operator==(const CircularInt& c, int n) {
	return c.now()==n; }
