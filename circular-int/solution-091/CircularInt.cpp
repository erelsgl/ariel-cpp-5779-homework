#include "CircularInt.hpp"
#include <string>
using namespace std;


CircularInt::CircularInt(int min, int max, int now): 
	_min(min), _max(max), _now(now), _count(max-min+1) {
	if (min>max)
		throw string("min>max: min=")+to_string(min)+", max="+to_string(max);
}

int CircularInt::normalized(int num) const {
	num -= _min;
	num %= _count;
	if (num<0) num += _count;
	num += _min;
	return num;
}

const CircularInt operator/(const CircularInt& c, int n) {
	return c.modified(c.now() / n);
}
