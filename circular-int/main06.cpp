#include <iostream>
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "CircularInt.hpp"
#include "catch.hpp"

std::string cirToString(const CircularInt &circularInt)
{
    std::stringstream ss;
    ss << circularInt ;
    return ss.str();
}

TEST_CASE("multiply","*")
{
    CircularInt hour1 {1, 10};
    hour1 = 8;
    hour1 = hour1 * hour1; // 8*8=64
    REQUIRE(cirToString(hour1)=="4");

    CircularInt hour2 {1, 10};
    hour2 = 3;
    hour2 = hour1 * hour2 ; // 4*3=12
    REQUIRE(cirToString(hour2)=="2"); 

    hour2 = hour2 * 7 ; // 2*7=14
    REQUIRE(cirToString(hour2)=="4"); 
}

TEST_CASE("multiply negative","*")
{
    CircularInt hour1 {-10, -1};
    hour1 = 8;  // -2
    hour1 = hour1 * hour1; // 4
    REQUIRE(cirToString(hour1)=="-6");

    CircularInt hour2 {-10, -1};
    hour2 = 3;  // -7
    hour2 = hour1 * hour2 ; // 42
    REQUIRE(cirToString(hour2)=="-8"); 

    hour2 = hour2 * 7 ; // -56
    REQUIRE(cirToString(hour2)=="-6"); 
}
