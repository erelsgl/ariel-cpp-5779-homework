# Circular Int

Create a class CircularInt that represents an integer that cycles within a given range.

See the readme.pdf file for more details.

Required knowledge: C++ classes and operator overloading.

Credit: Amit Palti helped writing the automatic tests.
