
#include <iostream>
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "CircularInt.hpp"
#include "catch.hpp"

std::string cirToString(const CircularInt &circularInt)
{
    std::stringstream ss;
    ss << circularInt ;
    return ss.str();
}
TEST_CASE("print","printing current value")
{
    CircularInt hour {1, 12};
    std::stringstream ss;
    ss << hour ;
    REQUIRE(ss.str() == "1");
}
