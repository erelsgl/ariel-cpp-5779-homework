#include <iostream>
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "CircularInt.hpp"
#include "catch.hpp"

std::string cirToString(const CircularInt &circularInt)
{
    std::stringstream ss;
    ss << circularInt ;
    return ss.str();
}

TEST_CASE("Add","+=")
{
    CircularInt hour {1, 12};
    hour += 4;
    std::stringstream ss;
    ss << hour ;
    REQUIRE(ss.str() == "5");
    hour += hour ;
    std::stringstream ss3;
    ss3<<hour;
    REQUIRE(ss3.str() == "10");
}