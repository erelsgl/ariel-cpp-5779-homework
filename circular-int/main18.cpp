#include <iostream>
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "CircularInt.hpp"
#include "catch.hpp"

std::string cirToString(const CircularInt &circularInt)
{
    std::stringstream ss;
    ss << circularInt ;
    return ss.str();
}
TEST_CASE("smallerThen","<")
{
    CircularInt hour {1, 10};
    CircularInt hour2 {1, 10};
    hour = hour + 1 ;
    REQUIRE( hour2 < hour);
    REQUIRE( 1 < hour );
    REQUIRE( hour < 3);
}