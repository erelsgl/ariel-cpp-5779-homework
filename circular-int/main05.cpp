#include <iostream>
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "CircularInt.hpp"
#include "catch.hpp"

std::string cirToString(const CircularInt &circularInt)
{
    std::stringstream ss;
    ss << circularInt ;
    return ss.str();
}


TEST_CASE("submit","-=")
{
    CircularInt hour {1, 12};
    hour -= 4;
    std::stringstream ss;
    ss << hour ;
    REQUIRE(ss.str() == "9");
    CircularInt hour2 {1, 12};
    hour -= hour2 ;
    std::stringstream ss3;
    ss3<<hour;
    REQUIRE(ss3.str() == "8");
    std::stringstream ss4;
    ss4<<hour2;
    REQUIRE(ss4.str() == "1");
}
