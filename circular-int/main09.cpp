#include <iostream>
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "CircularInt.hpp"
#include "catch.hpp"

std::string cirToString(const CircularInt &circularInt)
{
    std::stringstream ss;
    ss << circularInt ;
    return ss.str();
}
TEST_CASE("decrese","--")
{
    CircularInt hour {1, 4};
    hour--;
    hour--;
    hour--;
    hour--;
    REQUIRE(cirToString(hour)=="1");
}