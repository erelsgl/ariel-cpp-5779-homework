#include <iostream>
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "CircularInt.hpp"
#include "catch.hpp"

std::string cirToString(const CircularInt &circularInt)
{
    std::stringstream ss;
    ss << circularInt ;
    return ss.str();
}

TEST_CASE("inputStream",">>")
{
    CircularInt hour {1, 10};
    std::stringstream("9")>>hour;
    REQUIRE(cirToString(hour)=="9");
    std::stringstream("31")>>hour;
    REQUIRE(cirToString(hour)=="1");
    std::stringstream("-6")>> hour;
    REQUIRE(cirToString(hour)=="4");
}
