#include <iostream>
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "CircularInt.hpp"
#include "catch.hpp"

std::string cirToString(const CircularInt &circularInt)
{
    std::stringstream ss;
    ss << circularInt ;
    return ss.str();
}

TEST_CASE("Assignment","=")
{
    CircularInt hour {1, 10};
    hour = 9;
    REQUIRE(cirToString(hour)=="9");
    hour = 31;
    REQUIRE(cirToString(hour)=="1");
    hour = -6 ;
    REQUIRE(cirToString(hour)=="4");
    CircularInt hour2 {1, 10};
    hour = hour2;
    REQUIRE(cirToString(hour)=="1");
}