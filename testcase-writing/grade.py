import re
GRADE_REGEXP = re.compile("assertions: ([0-9]*) . ([0-9]*) passed . ([0-9]*) failed", re.IGNORECASE)
GRADE2_REGEXP = re.compile("All tests passed .([0-9]*) assertion in ([0-9]*) test case.", re.IGNORECASE)
GRADE3_REGEXP = re.compile("All tests passed .([0-9]*) assertions in ([0-9]*) test case.*", re.IGNORECASE)
GRADE4_REGEXP = re.compile("assertions: ([0-9]*) . ([0-9]*) failed", re.IGNORECASE)
VALGRIND_REGEXP = re.compile(".*definitely lost. ([0-9]*) bytes in.*", re.IGNORECASE)
RESULTS_FILE="results.txt"
gradeLinePrefix =  "your grade is :"
with open(RESULTS_FILE) as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
total = 0.0
passed = 0.0
failed = 0.0

content = [x.strip() for x in content]
for line in content:
    if "assertions: - none -" in line:
        total = total + 1
        passed = passed + 1
    try:
        matches = GRADE_REGEXP.search(line)
        total = total + int(matches.group(1))
        passed = passed + int(matches.group(2))
        failed = failed + int(matches.group(3))
    except AttributeError:
        total = total
    try:
        matches = GRADE4_REGEXP.search(line)
        total = total + int(matches.group(1))
        passed = passed + 0
        failed = failed + int(matches.group(2))
    except AttributeError:
        total = total
    try:
        matches = GRADE2_REGEXP.search(line)
        total = total + int(matches.group(1))
        passed = passed + int(matches.group(1))
        failed = failed + 0
    except AttributeError:
        total = total
    try:
        matches = GRADE3_REGEXP.search(line)
        total = total + int(matches.group(1))
        passed = passed + int(matches.group(1))
        failed = failed + 0
    except AttributeError:
        total = total
    try:
        matches = VALGRIND_REGEXP.search(line)
        leak = int(matches.group(1))
        print(str(leak))
        if (leak == 0) :
           passed = passed + 1
        else :
           failed = failed + 1
        total = total + 1
    except AttributeError:
        continue

if total == 0 :
 total = 1 
studentGrade = (passed*100.0)/total
print("total ="+str(total) +", passed = " + str(passed))
print(gradeLinePrefix + str(studentGrade))


    




