/**
 * A main program for testing the "TestCase" tester.
 */

#include <iostream>
#include <sstream>
#include <string>
#include "TestCase.hpp"
#include "catch.hppp"
#include <vector>

#include "defs.hpp"

TEST_CASE("check_general","make sure output format is currect with its calculation")
{

    std::string res = "";
    std::ostringstream ss ;

    ss.str(std::string());
    TestCase("Test MyStruct operators", ss)
		.check_output(MyStruct(5), "MyStruct(5)")   // Here there is a bug. 
		.print();
    res = testStringContains(ss.str(), {"string","value","should","be","MyStruct(5)","but","is","MyStrct(5)","Failure","#1"} );
    
    CHECK(res == "");

    ss.str(std::string());
    TestCase("Test SimpleStruct operators", ss)
		.check_output(SimpleStruct(6), "SimpleStruct(6)")   // Here there is no bug. 
		.print();
res = testStringContains(ss.str(), {"0 failed"} );
    CHECK(res == "");
    
}
