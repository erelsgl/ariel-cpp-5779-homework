/**
 * A main program for testing the "TestCase" tester.
 */

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "catch.hppp"

/**
 * Below we define some functions with deliberate bugs, 
 * to demonstrate that our tester can find them.
 */

int sqr(int x) { 
	return x*x*x;   // a deliberate bug (it should be: x*x)
}

int round_func(double x) { 
	return int(x);  // a deliberate bug (it should be: int(x+0.5)).
}


/**
 * Below we define a whole new struct with deliberate bugs, 
 * to demonstrate that our tester can find bugs even in new classes.
 */

struct MyStruct {
	int num;
	MyStruct(int num): num(num) {}
	bool operator==(const MyStruct& other) const {
		return false; // a deliberate bug
	}
	bool operator!=(const MyStruct& other) const {
		return num!=other.num; // no bug 
	}
	int myNum() const { 
		return num+2;   // a deliberate bug
	}
};

struct SimpleStruct
{
    long num;
    	SimpleStruct(int num): num(num) {}
    bool operator==(const SimpleStruct& other) const {
		return num == other.num; 
	}
    bool operator!=(const SimpleStruct& other) const {
		return num != other.num; 
	}
    int myNum() const { 
		return num;   
	}
};

int getNum(const MyStruct& s) {
	return s.num+1; // a deliberate bug
}

long simpleGetNum(const SimpleStruct& s) {
	return s.num; // a deliberate bug
}
std::ostream& operator<< (std::ostream& out, const SimpleStruct& tc) {
	return (out << "SimpleStruct"<<"("<<tc.num<<")"); 
}
std::ostream& operator<< (std::ostream& out, const MyStruct& tc) {
	return (out << "MyStrct"<<"("<<tc.num<<")"); // a deliberate typo (forgot "u").
}

std::string styleText (std::string res )
{
	return "<div class='testcase'>"+ res +"</div>";
}
// test that testcase contains all the must contain words  

std::string testStringContains (std::string res , std::vector<std::string> mustContainWord)
{
	std::string out = "";
	for (auto word : mustContainWord) 
    {
		if(res.find(word) ==  std::string::npos)
		{
			
			if( out == "" )
			{
				out =  styleText("your testcase output was : ") + res + "\n";
				
				out= out + styleText("test case print must contain all the of the following:")+"\n" ;
				
				out= out + "     (*) '" + word + "'\n" ;
			}
			else
			{
				out= out + "     (*) '" + word + "'\n" ;
			}
		}
	}
	return out;
}
