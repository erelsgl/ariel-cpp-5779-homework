


/**
 * A main program for testing the "TestCase" tester.
 */

#include <iostream>
#include <sstream>
#include <string>
#include "TestCase.hpp"
#include "catch.hppp"
#include <vector>

#include "defs.hpp"

TEST_CASE("check_output","check_output")
{

    std::string res = "";
    std::ostringstream ss ;

    ss.str(std::string());
    TestCase("Test int operators", ss)
		.check_equal(5,5)                  // check operator ==. Here there is no bug.
		.check_different(5,6)              // check operator !=. Here there is no bug.
		.check_function(sqr, 1, 1)         // check a function int->int.     Here there is no bug.
		.check_function(sqr, 5, 25)        // check a function int->int.    Here there is a bug.
		.check_function(round_func, 5.3, 5)     // check a function double->int. Here there is no bug.
		.check_function(round_func, 5.8, 6)     // check a function double->int. Here there is a bug.
		.check_output(5, "5")     // check output operator <<
		.print();
    res = testStringContains(ss.str(), {"#4","#6","2 failed","5 passed","7 total"} );
    
    CHECK(res == "");

    ss.str(std::string());
    TestCase("Test MyStruct operators", ss)
		.check_equal(MyStruct(5), MyStruct(5))      // Here there is a bug.
		.check_different(MyStruct(5), MyStruct(6))  // Here there is no bug.
		.check_output(MyStruct(5), "MyStruct(5)")   // Here there is a bug. 
		.check_function(getNum, MyStruct(5), 5)     // Here there is a bug.
		.check_function([](const MyStruct& s){return s.myNum();}, MyStruct(5), 5) // Here there is a bug.
		.print();
	res = testStringContains(ss.str(), {"#1","#3","#4","#5","4 failed","1 passed","5 total"} );

    CHECK(res == "");
}