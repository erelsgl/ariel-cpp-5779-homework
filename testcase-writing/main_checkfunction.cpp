/**
 * A main program for testing the "TestCase" tester.
 */

#include <iostream>
#include <sstream>
#include <string>
#include "TestCase.hpp"
#include "catch.hppp"
#include <vector>
#include "defs.hpp"



TEST_CASE("check_function","check_function")
{
    std::string res = "";
    std::ostringstream ss ;
        ss.str(std::string());

    TestCase("Test int operators", ss)
		.check_function(sqr, 1, 1)         // check a function int->int.     Here there is no bug.
		.print();
res = testStringContains(ss.str(), {"0 failed"} );



    ss.str(std::string());
    TestCase("Test MyStruct operators", ss)
		.check_function(sqr, 5, 25)        // check a function int->int.    Here there is a bug.
		.print();
    res = testStringContains(ss.str(), {"Function","should","return","25","but","returned","125","Failure", "#1"} );
    
    CHECK(res == "");

    ss.str(std::string());
    TestCase("Test MyStruct operators", ss)
		.check_function(round_func, 5.8, 6)     // check a function double->int. Here there is a bug.
		.print();
    res = testStringContains(ss.str(), {"Function","should","return","6","but","returned","5","Failure", "#1"} );
    
    CHECK(res == "");

    ss.str(std::string());
    TestCase("Test SimpleStruct operators", ss)
		.check_function(round_func, 5.3, 5)     // check a function double->int. Here there is no bug.
		.print();
res = testStringContains(ss.str(), {"0 failed"} );
    
    CHECK(res == "");

    ss.str(std::string());
    TestCase("Test MyStruct operators", ss)
		.check_function(getNum, MyStruct(5), 5)     // Here there is a bug.
		.print();
    res = testStringContains(ss.str(), {"Function","should","return","6","but","returned","5","Failure", "#1"} );
    
    CHECK(res == "");
    ss.str(std::string());
    TestCase("Test MyStruct operators", ss)
		.check_function([](const MyStruct& s){return s.myNum();}, MyStruct(5), 5) // Here there is a bug.
		.print();
    res = testStringContains(ss.str(), {"Function","should","return","5","but","returned","7","Failure", "#1"} );
    
    CHECK(res == "");

    ss.str(std::string());
    TestCase("Test SimpleStruct operators", ss)
		.check_function(simpleGetNum, SimpleStruct(5), 5)     // Here there is no bug.
		.print();
    res = testStringContains(ss.str(), {"0 failed"} );
    
    CHECK(res == "");

    ss.str(std::string());
    TestCase("Test SimpleStruct operators", ss)
		.check_function([](const SimpleStruct& s){return s.myNum();}, SimpleStruct(5), 5) // Here there is a bug.
		.print();
    res = testStringContains(ss.str(), {"0 failed"} );
    
    CHECK(res == "");

}