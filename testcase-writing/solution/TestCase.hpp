#pragma once

#include <string>
#include <iostream>
using std::string;
using std::ostream;
using std::endl;
using std::cerr;
using std::istringstream;
using std::ostringstream;

class TestCase {
	string name;        // name of test-case.
	int passed, failed, total; // keeps the number of tests passed/failed so far.
	ostream& output;    // where all output will be sent.

	bool fails(bool passes) {
		if (passes) {
			passed++;
			total++;
			return false;
		} else {
			failed++;
			total++;
			output << name << ": " << "Failure in test #" << total<< ": ";
			return true;
		}
	}

public:
	ostream& print(ostream& out) const {
		return (out << name << ": " << failed << " failed, " << passed << " passed, " << total << " total.");
	}
	TestCase& print()  {
		print(this->output);
		this->output << endl << "---" << endl;
		return *this;
	}
	TestCase(const string& name, ostream& output=cerr): 
		name(name), 
		output(output),
		passed(0), failed(0), total(0) {}

	template<typename T> TestCase& check_equal(const T& a, const T& b) {
		if (fails(a==b))
			output << a << " should equal " << b << "!" << endl;
		return *this;
	}

	template<typename T> TestCase& check_different(const T& a, const T& b) {
		if (fails(a!=b))
			output << a << " should differ than " << b << "!" << endl;
		return *this;
	}

	template<typename T> TestCase& check_output(const T& a, const string& expected) {
		ostringstream ostr;
		ostr << a;
		string actual = ostr.str();
		if (fails(actual==expected))
			output << "string value should be " << expected << " but is " << actual << endl;
		return *this;
	}

	template<typename T> TestCase& check_input(const string& input, const T& expected) {
		istringstream istr(input);
		T actual;
		istr >> actual;
		if (fails(actual==expected))
			output << "object generated from " << input << " should be " << expected << " but is " << actual << endl;
		return *this;
	}

	template<typename Function, typename Argument, typename Result> TestCase& check_function(Function f, Argument argument, Result expected) {
		Result actual =f(argument); 
		if (fails(actual==expected))
			output << "Function should return " << expected << " but returned " << actual << "!" << endl;
		return *this;
	}
};

