/**
 * A main program for testing the "TestCase" tester.
 */

#include <iostream>
#include <sstream>
#include <string>
#include "TestCase.hpp"
#include "catch.hppp"
#include <vector>
#include "defs.hpp"


TEST_CASE("equals","check operator ==")
{
    std::string res = "";
    std::ostringstream ss ;
	    ss.str(std::string());

    TestCase("Test int operators", ss)
		.check_equal(5,5)                  // check operator ==. Here there is no bug.
		.print();
    res = testStringContains(ss.str(), {"0 failed"} );

    CHECK(res == "");

    ss.str(std::string());
    TestCase("Test MyStruct operators", ss)
		.check_equal(MyStruct(5), MyStruct(5))      // Here there is a bug.
		.print();
    res = testStringContains(ss.str(), {"MyStrct(5)","should","equal","Failure", "#1"} );
    
    CHECK(res == "");

    ss.str(std::string());
    TestCase("Test SimpleStruct operators", ss)
		.check_equal(SimpleStruct(5), SimpleStruct(5))      // Here there is no bug.
		.print();
        res = testStringContains(ss.str(), {"0 failed"} );
    
    CHECK(res == "");

}