#include <iostream>
#include <string>
#include "catch.hppp"
#include "Player.h"
#include "DummyPlayers.h"
#include "Board.h"
#include "Champion.h"
#include <sstream>
#include "TicTacToe.h"
std::string BoardToString(const Board & b)
{
    std::stringstream ss;
    ss << b ;
    return ss.str();
}
void printResults(const TicTacToe& game) {
	cout << endl << "The final board is " << endl << game.board();
	cout << "And the winner is " << game.winner().name() 
	     << ", playing as " << game.winner().getChar() << "!" << endl;
}

void playAndPrintResults(TicTacToe& game, Player& xPlayer, Player& oPlayer) {
	game.play(xPlayer, oPlayer);
	printResults(game);
}

void playAndPrintWinner(TicTacToe& game, Player& xPlayer, Player& oPlayer) {
	game.play(xPlayer, oPlayer);
	cout << "The winner is " << game.winner().name() 
	     << ", playing as " << game.winner().getChar() << "!" << endl;
}
class TiePlayer: public Player {
public:
	const string name() const override { return "TiePlayer"; }
	const Coordinate play(const Board& board) override;
};
const Coordinate TiePlayer::play(const Board& board) {
	if ((board[{0,0}]!='.')&&(board[{0,1}]=='.'))
		return {0,1};
	if ((board[{0,2}]!='.')&&(board[{1,0}]=='.'))
		return {1,0};
	if ((board[{1,1}]!='.')&&(board[{1,2}]=='.'))
		return {2,1};
	if ((board[{2,1}]!='.')&&(board[{2,2}]=='.'))
		return {2,1};
	return {0,0};  // did not find an empty square - play on the top-left
}

TEST_CASE("Player_constraction","player_ctor")
{
    XYPlayer xyPl ;
    CHECK(true);
    
}

TEST_CASE("TicTacToe_play1","TicTacToe_play1")
{
    TicTacToe game(4);       // Initializes a game on a 4x4 board
	XYPlayer player1;
  XYPlayer player1_copy;
	YXPlayer player2;
	IllegalPlayer player3;
	ExceptionPlayer player4;
    TiePlayer player5;
    TicTacToe largeGame(8);       // Initializes a game on a 8x8 board

	game.play( player1, player2);
	/*
		The final board is 
		XXXX
		O...
		O...
		O...
    	And the winner is XYPlayer, playing as X!
	*/
    CHECK(BoardToString(game.board()) == "XXXX\nO...\nO...\nO...\n");
    CHECK(game.winner().name() == "XYPlayer");
    CHECK(game.winner().getChar() == 'X');

	largeGame.play(player1, player2);
	/*
		The final board is 
		XXXXXXXX
		O.......
		O.......
		O.......
		O.......
		O.......
		O.......
		O.......
    	And the winner is XYPlayer, playing as X!
	*/
    CHECK(BoardToString(largeGame.board()) == "XXXXXXXX\nO.......\nO.......\nO.......\nO.......\nO.......\nO.......\nO.......\n");
    CHECK(largeGame.winner().name() == "XYPlayer");
    CHECK(largeGame.winner().getChar() == 'X');

    game.play( player2, player1);  
	/*
		The final board is 
		XOOO
		X...
		X...
		X...
        And the winner is YXPlayer, playing as X!
	*/
    CHECK(BoardToString(game.board()) == "XOOO\nX...\nX...\nX...\n");
    CHECK(game.winner().name() == "YXPlayer");
    CHECK(game.winner().getChar() == 'X');

	game.play( player1, player3);  
	/*
		The final board is 
		X...
		....
		....
		....
        And the winner is XYPlayer, playing as X!
	*/
    CHECK(BoardToString(game.board()) == "X...\n....\n....\n....\n");
    CHECK(game.winner().name() == "XYPlayer");
    CHECK(game.winner().getChar() == 'X');



	game.play( player3, player1);  
	/*
		The final board is 
		XO..
		....
		....
		....
        And the winner is XYPlayer, playing as O!
	*/
    CHECK(BoardToString(game.board()) == "XO..\n....\n....\n....\n");
    CHECK(game.winner().name() == "XYPlayer");
    CHECK(game.winner().getChar() == 'O');

	game.play( player2, player4);  
	/*
		The final board is 
		X...
		....
		....
		....
		And the winner is YXPlayer, playing as X!
	*/
    CHECK(BoardToString(game.board()) == "X...\n....\n....\n....\n");
    CHECK(game.winner().name() == "YXPlayer");
    CHECK(game.winner().getChar() == 'X');

	game.play( player4, player2);  
	/*
		The final board is 
		X...
		....
		....
		....
		And the winner is YXPlayer, playing as O!
	*/
    CHECK(BoardToString(game.board()) == "X...\n....\n....\n....\n");
    CHECK(game.winner().name() == "YXPlayer");
    CHECK(game.winner().getChar() == 'O');

    game.play( player1, player1_copy);  
 
	/*
		The final board is 
		XOXO
		XOXO
		XOXO
		X...
		And the winner is YXPlayer, playing as X!
	*/
    CHECK(BoardToString(game.board()) == "XOXO\nXOXO\nXOXO\nX...\n");
    CHECK(game.winner().name() == "XYPlayer");
    CHECK(game.winner().getChar() == 'X');
    
    TicTacToe medGame(5);       // Initializes a game on a 5x5 board
    medGame.play( player1, player1_copy);  
	/*
		The final board is 
		XOXOX
        OXOXO
        XOXOX
        OXOXO
        X....
		And the winner is YXPlayer, playing as X!
	*/
    CHECK(BoardToString(medGame.board()) == "XOXOX\nOXOXO\nXOXOX\nOXOXO\nX....\n");
    CHECK(medGame.winner().name() == "XYPlayer");
    CHECK(medGame.winner().getChar() == 'X');

    TicTacToe smallGame(3);       // Initializes a game on a 3x3 board
    smallGame.play( player1, player5);  
	/*
		The final board is 
		XOX
    OXX
    OXO
		And the winner is tiePlayer, playing as O!
	*/
    CHECK(BoardToString(smallGame.board()) == "XOX\nOXX\nOXO\n");
    CHECK(smallGame.winner().name() == "TiePlayer");
    CHECK(smallGame.winner().getChar() == 'O');

}



TEST_CASE("Champion","Real Champion wins all the times")
{
    TicTacToe game(7);       // Initializes a game on a 4x4 board
	XYPlayer player1;
	YXPlayer player2;
	IllegalPlayer player3;
	ExceptionPlayer player4;
	Champion champion;    
    std::string yourName = champion.name();

    game.play( champion, player1);
		// The winner is <your name>, playing as X!
    CHECK(game.winner().name() == yourName);
    CHECK(game.winner().getChar() == 'X');
    
	game.play( player1, champion);
		// The winner is <your name>, playing as O!
    CHECK(game.winner().name() == yourName);
    CHECK(game.winner().getChar() == 'O');

	game.play( champion, player2);
		// The winner is <your name>, playing as X!
    CHECK(game.winner().name() == yourName);
    CHECK(game.winner().getChar() == 'X');

	game.play( player2, champion);
		// The winner is <your name>, playing as O!
    CHECK(game.winner().name() == yourName);
    CHECK(game.winner().getChar() == 'O');

	game.play( champion, player3);
		// The winner is <your name>, playing as X!
    CHECK(game.winner().name() == yourName);
    CHECK(game.winner().getChar() == 'X');

	game.play( player3, champion);
		// The winner is <your name>, playing as O!
    CHECK(game.winner().name() == yourName);
    CHECK(game.winner().getChar() == 'O');

	game.play( champion, player4);
		// The winner is <your name>, playing as X!
    CHECK(game.winner().name() == yourName);
    CHECK(game.winner().getChar() == 'X');

	game.play( player4, champion);
        // The winner is <your name>, playing as O!
    CHECK(game.winner().name() == yourName);
    CHECK(game.winner().getChar() == 'O');
}